class GrossRevenuesController < ApplicationController
  before_action :set_gross_revenue, only: [:show, :edit, :update, :destroy]

  # GET /gross_revenues
  # GET /gross_revenues.json
  def index
    @gross_revenues = GrossRevenue.all
  end

  # GET /gross_revenues/1
  # GET /gross_revenues/1.json
  def show
  end

  # GET /gross_revenues/new
  def new
    @gross_revenue = GrossRevenue.new
  end

  # GET /gross_revenues/1/edit
  def edit
  end

  # POST /gross_revenues
  # POST /gross_revenues.json
  def create
    @gross_revenue = GrossRevenue.new(gross_revenue_params)

    respond_to do |format|
      if @gross_revenue.save
        format.html { redirect_to @gross_revenue, notice: 'Gross revenue was successfully created.' }
        format.json { render action: 'show', status: :created, location: @gross_revenue }
      else
        format.html { render action: 'new' }
        format.json { render json: @gross_revenue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gross_revenues/1
  # PATCH/PUT /gross_revenues/1.json
  def update
    respond_to do |format|
      if @gross_revenue.update(gross_revenue_params)
        format.html { redirect_to @gross_revenue, notice: 'Gross revenue was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @gross_revenue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gross_revenues/1
  # DELETE /gross_revenues/1.json
  def destroy
    @gross_revenue.destroy
    respond_to do |format|
      format.html { redirect_to gross_revenues_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gross_revenue
      @gross_revenue = GrossRevenue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gross_revenue_params
      params[:gross_revenue]
    end
end
