class TotalRevenuesController < ApplicationController
  before_action :set_total_revenue, only: [:show, :edit, :update, :destroy]

  # GET /total_revenues
  # GET /total_revenues.json
  def index
    @total_revenues = TotalRevenue.all
  end

  # GET /total_revenues/1
  # GET /total_revenues/1.json
  def show
  end

  # GET /total_revenues/new
  def new
    @total_revenue = TotalRevenue.new
  end

  # GET /total_revenues/1/edit
  def edit
  end

  # POST /total_revenues
  # POST /total_revenues.json
  def create
    @total_revenue = TotalRevenue.new(total_revenue_params)

    respond_to do |format|
      if @total_revenue.save
        format.html { redirect_to @total_revenue, notice: 'Total revenue was successfully created.' }
        format.json { render action: 'show', status: :created, location: @total_revenue }
      else
        format.html { render action: 'new' }
        format.json { render json: @total_revenue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /total_revenues/1
  # PATCH/PUT /total_revenues/1.json
  def update
    respond_to do |format|
      if @total_revenue.update(total_revenue_params)
        format.html { redirect_to @total_revenue, notice: 'Total revenue was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @total_revenue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /total_revenues/1
  # DELETE /total_revenues/1.json
  def destroy
    @total_revenue.destroy
    respond_to do |format|
      format.html { redirect_to total_revenues_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_total_revenue
      @total_revenue = TotalRevenue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def total_revenue_params
      params.require(:total_revenue).permit(:sale_id, :total_gross_revenue)
    end
end
