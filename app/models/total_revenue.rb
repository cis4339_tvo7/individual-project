class TotalRevenue < ActiveRecord::Base
  has_many :sales

  def total_g_revenue
    total_count = 0
    Sale.all.each do |c|
      total_count += c.revenue
    end
    total_count
  end

end
