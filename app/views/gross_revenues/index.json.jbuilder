json.array!(@gross_revenues) do |gross_revenue|
  json.extract! gross_revenue, :id
  json.url gross_revenue_url(gross_revenue, format: :json)
end
