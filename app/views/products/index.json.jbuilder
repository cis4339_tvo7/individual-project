json.array!(@products) do |product|
  json.extract! product, :id, :name, :quanity, :price, :minimum
  json.url product_url(product, format: :json)
end
