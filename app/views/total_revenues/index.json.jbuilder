json.array!(@total_revenues) do |total_revenue|
  json.extract! total_revenue, :id, :sale_id, :total_gross_revenue
  json.url total_revenue_url(total_revenue, format: :json)
end
