# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Product.create(name: 'Mac', quanity: 242, price: 1055.00, minimum: 4)
Product.create(name: 'iPhone', quanity: 42, price: 105.00, minimum: 2)
Product.create(name: 'GPU', quanity: 2, price: 550.00, minimum: 7)
Product.create(name: 'Doors', quanity: 35, price: 15.00, minimum: 4)
Product.create(name: 'Mac & Cheese', quanity: 242, price: 5.00, minimum: 6)

Sale.create(product_id: 1, sold: 3, price: nil, date: 02/01/2014)
Sale.create(product_id: 4, sold: 3, price: nil, date: 03/01/2014)
Sale.create(product_id: 3, sold: 6, price: nil, date: 04/01/2014)
Sale.create(product_id: 2, sold: 2, price: nil, date: 05/01/2014)

Order.create(product_id: 1, order_quanity: 3424,  order_date: 02/01/2014)
Order.create(product_id: 4, order_quanity: 3242,  order_date: 03/01/2014)
Order.create(product_id: 3, order_quanity: 644,  order_date: 04/01/2014)
Order.create(product_id: 2, order_quanity: 200,  order_date: 05/01/2014)
