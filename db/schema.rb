# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140321034603) do

  create_table "gross_revenues", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.integer  "product_id"
    t.integer  "order_quanity"
    t.date     "order_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.integer  "quanity"
    t.float    "price"
    t.integer  "minimum"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales", force: true do |t|
    t.integer  "product_id"
    t.integer  "sold"
    t.float    "price"
    t.date     "date"
    t.float    "revenue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "total_revenues", force: true do |t|
    t.integer  "sale_id"
    t.float    "total_gross_revenue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
