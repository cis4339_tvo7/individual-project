class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :product_id
      t.integer :sold
      t.float :price
      t.date :date
      t.float :revenue

      t.timestamps
    end
  end
end
