class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :product_id
      t.integer :order_quanity
      t.date :order_date

      t.timestamps
    end
  end
end
