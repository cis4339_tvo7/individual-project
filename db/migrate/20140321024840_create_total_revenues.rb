class CreateTotalRevenues < ActiveRecord::Migration
  def change
    create_table :total_revenues do |t|
      t.integer :sale_id
      t.float :total_gross_revenue

      t.timestamps
    end
  end
end
