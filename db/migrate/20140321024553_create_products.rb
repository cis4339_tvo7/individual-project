class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :quanity
      t.float :price
      t.integer :minimum

      t.timestamps
    end
  end
end
