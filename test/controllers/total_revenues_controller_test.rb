require 'test_helper'

class TotalRevenuesControllerTest < ActionController::TestCase
  setup do
    @total_revenue = total_revenues(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:total_revenues)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create total_revenue" do
    assert_difference('TotalRevenue.count') do
      post :create, total_revenue: { sale_id: @total_revenue.sale_id, total_gross_revenue: @total_revenue.total_gross_revenue }
    end

    assert_redirected_to total_revenue_path(assigns(:total_revenue))
  end

  test "should show total_revenue" do
    get :show, id: @total_revenue
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @total_revenue
    assert_response :success
  end

  test "should update total_revenue" do
    patch :update, id: @total_revenue, total_revenue: { sale_id: @total_revenue.sale_id, total_gross_revenue: @total_revenue.total_gross_revenue }
    assert_redirected_to total_revenue_path(assigns(:total_revenue))
  end

  test "should destroy total_revenue" do
    assert_difference('TotalRevenue.count', -1) do
      delete :destroy, id: @total_revenue
    end

    assert_redirected_to total_revenues_path
  end
end
