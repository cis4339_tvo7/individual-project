require 'test_helper'

class GrossRevenuesControllerTest < ActionController::TestCase
  setup do
    @gross_revenue = gross_revenues(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gross_revenues)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gross_revenue" do
    assert_difference('GrossRevenue.count') do
      post :create, gross_revenue: {  }
    end

    assert_redirected_to gross_revenue_path(assigns(:gross_revenue))
  end

  test "should show gross_revenue" do
    get :show, id: @gross_revenue
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gross_revenue
    assert_response :success
  end

  test "should update gross_revenue" do
    patch :update, id: @gross_revenue, gross_revenue: {  }
    assert_redirected_to gross_revenue_path(assigns(:gross_revenue))
  end

  test "should destroy gross_revenue" do
    assert_difference('GrossRevenue.count', -1) do
      delete :destroy, id: @gross_revenue
    end

    assert_redirected_to gross_revenues_path
  end
end
